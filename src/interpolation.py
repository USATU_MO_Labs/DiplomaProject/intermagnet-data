from typing import NamedTuple, Iterable, List, Tuple
from pykrige.ok import OrdinaryKriging
import numpy as np


def generate_grid(longitudes: Iterable[float], latitudes: Iterable[float],
                  values: Iterable[float],
                  grid_step: int) -> Tuple[List[List[float]]]:
    ok = OrdinaryKriging(longitudes,
                         latitudes,
                         values,
                         variogram_model="spherical")
    z, ss = ok.execute("grid", np.arange(-180.0, 180.0, grid_step),
                       np.arange(0.0, 180.0, grid_step))

    return z.tolist(), ss.tolist()
