import cdflib
from geomagio.iaga2002 import IAGA2002Factory
from geomagio.algorithm import XYZAlgorithm, AlgorithmException
from obspy.core import Stats, Stream, Trace, UTCDateTime
from pathlib import Path
from datetime import date
import re
import itertools
from typing import NamedTuple, List
import enum


class FileFormat(enum.Enum):
    CDF = 0
    IAGA2002 = 1


class TimeSeriesRecord(NamedTuple):
    X: float
    Y: float
    Z: float
    F: float
    hour: int


class File(NamedTuple):
    format: FileFormat
    filename: str
    observatory: str
    year: int
    month: int
    day: int
    date: date


class ProcessedFile(NamedTuple):
    file: File
    timeseries: List[TimeSeriesRecord]


class FileReader:
    def __init__(self):
        self.__iaga_factory = IAGA2002Factory()
        self.__xyz_algo = XYZAlgorithm(informat='mag', outformat='geo')

    def process_file(self, path: Path) -> ProcessedFile:
        '''
        Read file, parse it's name and contents, and downsize contained
        time series.
        '''
        file = self.__process_filename(path)

        timeseries = None
        if file.format == FileFormat.CDF:
            timeseries = self.__read_cdf(path)
        elif file.format == FileFormat.IAGA2002:
            timeseries = self.__read_iaga2002(path)
        else:
            raise NotImplementedError('Format unsupported!')

        return ProcessedFile(file=file, timeseries=timeseries)

    def __process_filename(self, file_path: Path) -> File:
        '''
        Parses file names.
        '''
        # ImagCDF file name format
        # abk_20160719_0000_pt1m_3.cdf
        # last 3 matches have unknown purpose
        cdf_match = re.match(
            r'([a-z]{3})_(\d{4})(\d{2})(\d{2})_(\d{4})_(\w{4})_(\d)\.cdf',
            file_path.name)

        # IAGA2002 file name format
        # eyr20140115qmin.min
        iaga_match = re.match(r'([a-z]{3})(\d{4})(\d{2})(\d{2})qmin.min',
                              file_path.name)

        if cdf_match:
            fileformat = FileFormat.CDF
            filename = cdf_match[0]
            observatory = cdf_match[1]
            year = cdf_match[2]
            month = cdf_match[3]
            day = cdf_match[4]

        elif iaga_match:
            fileformat = FileFormat.IAGA2002
            filename = iaga_match[0]
            observatory = iaga_match[1]
            year = iaga_match[2]
            month = iaga_match[3]
            day = iaga_match[4]

        else:
            raise Exception('Unknown file format `{}`'.format(file_path))

        year = int(year)
        month = int(month)
        day = int(day)

        return File(format=fileformat,
                    filename=filename,
                    observatory=observatory,
                    year=year,
                    month=month,
                    day=day,
                    date=date(year, month, day))

    def __parse_geomag_stream(self, stream: Stream) -> List[TimeSeriesRecord]:
        '''
        Converts stream to XYZ if needed and downsizes the data by averaging
        observations for each hour.
        '''

        try:
            stream = self.__xyz_algo.process(stream)
        except AlgorithmException:
            pass

        X = Y = Z = F = None
        for trace in stream:
            c = trace.stats.channel

            if c == 'X':
                X = trace
            elif c == 'Y':
                Y = trace
            elif c == 'Z':
                Z = trace
            elif c == 'F':
                F = trace

        def downsize_trace(t):
            t0 = t.stats.starttime
            series = []
            for i in range(0, 24):
                a = t0 + 60 * 60 * i
                b = a + 60 * 59 + 59
                series.append(t.slice(a, b, False).data.mean())

            return series

        hourly_records = []
        downsized = tuple(map(downsize_trace, [X, Y, Z, F]))
        for (x, y, z, f, h) in zip(*downsized, itertools.count()):
            rec = TimeSeriesRecord(X=x, Y=y, Z=z, F=f, hour=h)
            hourly_records.append(rec)

        return hourly_records

    def __read_iaga2002(self, path: Path) -> List[TimeSeriesRecord]:
        '''
        Read IAGA2002 file.
        '''

        data = self.__iaga_factory.parse_string(path.read_text())
        return self.__parse_geomag_stream(data)

    def __read_cdf(self, path: Path) -> List[TimeSeriesRecord]:
        cdf_file = cdflib.CDF(str(path))

        # get time range
        times = cdf_file.varget('GeomagneticVectorTimes')

        tmin = cdflib.cdfepoch.breakdown_tt2000(times.min())
        tmin = (tmin[0], tmin[1], tmin[2], tmin[3], tmin[4], tmin[5])
        # tmax = cdflib.cdfepoch.breakdown_tt2000(times.max())
        # tmax = (tmax[0], tmax[1], tmax[2], tmax[3], tmax[4], tmax[5])

        cdf_info = cdf_file.cdf_info()

        mag_vars = filter(lambda v: v.startswith('GeomagneticField'),
                          cdf_info['zVariables'])

        stream = Stream()

        for var in mag_vars:
            data = cdf_file.varget(var)

            stats = Stats()
            stats.sampling_rate = 1 / 60
            stats.npts = len(data)
            stats.channel = var[-1]
            stats.starttime = UTCDateTime(*tmin)
            # stats.endtime = UTCDateTime(*tmax)

            stream += Trace(data, stats)

        return self.__parse_geomag_stream(stream)


if __name__ == '__main__':
    fr = FileReader()
    root = Path(__file__).parent / '..'
    intermagnet = root / Path(
        'testdata/ftp.seismo.nrcan.gc.ca/intermagnet/minute/quasi-definitive')

    # p_iaga_xyz = intermagnet / Path('IAGA2002/2019/04/bfo20190401qmin.min')
    # p_iaga_hdz = intermagnet / Path('IAGA2002/2019/04/asc20190401qmin.min')
    p_cdf_1 = intermagnet / Path('CDF/2018/01/01/asc_20180101_0000_pt1m_3.cdf')
    p_cdf_2 = intermagnet / Path('CDF/2019/04/01/abk_20190401_0000_pt1m_3.cdf')

    # print(fr.process_file(p_iaga_xyz))
    # print(fr.process_file(p_iaga_hdz))
    print(fr.process_file(p_cdf_1))
    print(fr.process_file(p_cdf_2))
