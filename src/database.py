import sqlite3
from pathlib import Path
import json
import msgpack
from typing import NamedTuple, Iterable, List
from .filereader import ProcessedFile


class Observatory(NamedTuple):
    iaga_code: str
    name: str
    country: str
    colatitude: float
    longitude: float
    institute: str
    GIN: str


class DBTimeSeriesRecord(NamedTuple):
    year: int
    month: int
    day: int
    hour: int
    iaga_code: int
    X: float
    Y: float
    Z: float
    F: float


class DBTimeSeriesDate(NamedTuple):
    year: int
    month: int
    day: int
    hour: int


class Grid(NamedTuple):
    year: int
    month: int
    day: int
    hour: int
    component: str
    grid: List[List[float]]
    grid_var: List[List[float]]


class DBGridRecord(NamedTuple):
    year: int
    month: int
    day: int
    hour: int
    component: str
    grid_msgpack: bytes
    grid_var_msgpack: bytes


class DatabaseService:
    def __init__(self, dbfile: Path):
        observatories_file = Path(__file__).parent / "observatories.json"
        with open(observatories_file) as f:
            self.observatories = json.load(f)

        self._db = sqlite3.connect(dbfile.absolute().as_uri(), uri=True)
        self.__init_db()

    def __init_db(self):
        """
        Create tables in database if they don't exist.
        """

        self.__create_table_observatories()
        self.__create_table_time_series()
        self.__create_table_grid()

    def __create_table_observatories(self):
        c = self._db.cursor()

        # create table for observatories
        c.execute("""
            create table if not exists observatories (
                iaga_code text primary key,
                name text not null,
                country text not null,
                latitude real not null,
                longitude real not null
            )
        """)

        table = c.execute("select * from observatories")
        result_count = sum(1 for row in table)

        if (result_count == 0):
            c.executemany(
                """
                insert into observatories (
                    iaga_code,
                    name,
                    country,
                    latitude,
                    longitude
                ) values (?, ?, ?, ?, ?)
            """, ((obs["IAGA"], obs["Name"], obs["Country"], obs["Colatitude"],
                   obs["East Longitude"]) for obs in self.observatories))

        self._db.commit()

    def __create_table_time_series(self):
        c = self._db.cursor()

        # create table for time series data
        c.execute("""
            create table if not exists time_series (
                year integer,
                month integer,
                day integer,
                hour integer,
                iaga_code text,
                x real not null,
                y real not null,
                z real not null,
                f real not null,
                primary key (year, month, day, hour, iaga_code),
                foreign key (iaga_code) references observatories (iaga_code)
            )
        """)

        self._db.commit()

    def __create_table_grid(self):
        c = self._db.cursor()

        # create table for time series data
        c.execute("""
            create table if not exists grid (
                year integer,
                month integer,
                day integer,
                hour integer,
                component text,
                grid_msgpack blob,
                grid_var_msgpack blob,
                primary key (year, month, day, hour, component)
            )
        """)

        self._db.commit()

    def close(self):
        self._db.commit()
        self._db.close()

    def get_observatories(self) -> Iterable[Observatory]:
        return map(
            lambda o: Observatory(iaga_code=o["IAGA"],
                                  name=o["Name"],
                                  country=o["Country"],
                                  colatitude=o["Colatitude"],
                                  longitude=o["East Longitude"],
                                  institute=o["Institute"],
                                  GIN=o["GIN"]), self.observatories)

    def __insert_time_series_records(self,
                                     records: Iterable[DBTimeSeriesRecord]):
        c = self._db.cursor()

        c.executemany(
            """
            insert or ignore into time_series (
                year, month, day, hour, iaga_code,
                x, y, z, f
            ) values (?, ?, ?, ?, ?, ?, ?, ?, ?)
        """, ((r.year, r.month, r.day, r.hour, r.iaga_code, r.X, r.Y, r.Z, r.F)
              for r in records))

        self._db.commit()

    def insert_file(self, f: ProcessedFile):
        records = (DBTimeSeriesRecord(year=f.file.year,
                                      month=f.file.month,
                                      day=f.file.day,
                                      iaga_code=f.file.observatory,
                                      hour=r.hour,
                                      X=r.X,
                                      Y=r.Y,
                                      Z=r.Z,
                                      F=r.F) for r in f.timeseries)

        self.__insert_time_series_records(records)

    def get_time_series(self, year, month, day,
                        hour) -> Iterable[DBTimeSeriesRecord]:
        c = self._db.cursor()

        c.execute(
            """
            select year, month, day, hour, iaga_code,
                x, y, z, f
            from time_series
            where year=? and month=? and day=? and hour=?
            """, (year, month, day, hour))

        return map(
            lambda r: DBTimeSeriesRecord(year=r[0],
                                         month=r[1],
                                         day=r[2],
                                         hour=r[3],
                                         iaga_code=r[4],
                                         X=r[5],
                                         Y=r[6],
                                         Z=r[7],
                                         F=r[8]), c)

    def get_time_series_dates(self) -> Iterable[DBTimeSeriesDate]:
        c = self._db.cursor()

        c.execute("select distinct year, month, day, hour from time_series")

        return map(
            lambda d: DBTimeSeriesDate(
                year=d[0], month=d[1], day=d[2], hour=d[3]), c)

    def __insert_grid_record(self, record: DBGridRecord):
        c = self._db.cursor()

        c.execute(
            """
            insert or ignore into grid (
                year, month, day, hour, component,
                grid_msgpack, grid_var_msgpack
            ) values (?, ?, ?, ?, ?, ?, ?)
            """,
            (record.year, record.month, record.day, record.hour,
             record.component, record.grid_msgpack, record.grid_var_msgpack))

        self._db.commit()

    def insert_grid(self, grid: Grid):
        record = DBGridRecord(year=grid.year,
                              month=grid.month,
                              day=grid.day,
                              hour=grid.hour,
                              component=grid.component,
                              grid_msgpack=msgpack.packb(grid.grid),
                              grid_var_msgpack=msgpack.packb(grid.grid_var))

        self.__insert_grid_record(record)

    def get_grid(self, year, month, day, hour, component) -> Grid:
        c = self._db.cursor()

        c.execute(
            """
            select year, month, day, hour, component,
                grid_msgpack, grid_var_msgpack
            from grid
            where year=? and month=? and day=? and hour=? and component=?
            """, (year, month, day, hour, component))

        r = c.fetchone()
        return Grid(year=r[0],
                    month=r[1],
                    day=r[2],
                    hour=r[3],
                    component=r[4],
                    grid=msgpack.unpackb(r[5]),
                    grid_var=msgpack.unpackb(r[6]))
