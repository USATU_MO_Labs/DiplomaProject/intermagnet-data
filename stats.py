from typing import List, Dict
import sys
from pathlib import Path
import re
from collections import defaultdict
from datetime import date
from pprint import pprint


def main():
    root_path = "." if len(sys.argv) == 1 else sys.argv[0]

    files = []
    print("Processing ImagCDF files...")
    for file in Path(root_path).glob("**/*.cdf"):
        files.append(process_file(file))

    print("Processing IAGA2002 files...")
    for file in Path(root_path).glob("**/*.min"):
        files.append(process_file(file))

    stats = calculate_stats(files)

    with open("stats.txt", "w") as f:
        pprint(stats, f)

    return 0


def process_file(file_path: Path):
    # ImagCDF file name format
    # abk_20160719_0000_pt1m_3.cdf
    # last 3 matches have unknown purpose
    cdf_match = re.match(
        r"([a-z]{3})_(\d{4})(\d{2})(\d{2})_(\d{4})_(\w{4})_(\d)\.cdf",
        file_path.name)

    # IAGA2002 file name format
    # eyr20140115qmin.min
    iaga_match = re.match(r"([a-z]{3})(\d{4})(\d{2})(\d{2})qmin.min",
                          file_path.name)

    if cdf_match:
        filetype = "cdf"
        filename = cdf_match[0]
        observatory = cdf_match[1]
        year = cdf_match[2]
        month = cdf_match[3]
        day = cdf_match[4]

    elif iaga_match:
        filetype = "iaga"
        filename = iaga_match[0]
        observatory = iaga_match[1]
        year = iaga_match[2]
        month = iaga_match[3]
        day = iaga_match[4]

    else:
        raise Exception("Unknown file format `{}`".format(file_path))

    year = int(year)
    month = int(month)
    day = int(day)

    file = {}
    file["type"] = filetype
    file["filename"] = filename
    file["observatory"] = observatory
    file["year"] = year
    file["month"] = month
    file["day"] = day
    file["date"] = date(year, month, day)

    return file


def calculate_stats(files: List[Dict]):
    """
    Caclulates stats from CDF file names

    Structure:

    stats = {
        total_days: int
        total_cdf: int
        total_iaga: int

        dates: {
            [date: date]: {
                observatories: set()
                count: {
                    total: int
                    cdf: int
                    iaga: int
                }
            }
        },

        observatories: {
            [observatory: str]: {
                total: {
                    num_days: int
                    fill_ratio: float   # num_days / total_days
                }
                cdf: {
                    num_days: int
                    fill_ratio: float
                }
                iaga: {
                    num_days: int
                    fill_ratio: float
                }
            }
        }
    }
    """

    def recursive_ddict():
        return defaultdict(recursive_ddict)

    stats = recursive_ddict()

    stats["total_days"] = len(set(map(lambda f: f["date"], files)))

    stats["total_cdf"] = 0
    stats["total_iaga"] = 0

    dates = stats["dates"]
    observatories = stats["observatories"]

    for file in files:
        filetype = file["type"]
        assert (filetype in ["cdf", "iaga"])

        stats["total_" + filetype] += 1

        date = file["date"]
        observatory = file["observatory"]

        date_dd = dates[date]

        date_dd["observatories"] = date_dd.get("observatories", set())
        date_dd["observatories"].add(observatory)

        date_dd["count"]["total"] = len(dates[date]["observatories"])
        date_dd["count"][filetype] = dates[date]["count"].get(filetype, 0) + 1

        obs_dd = observatories[observatory]

        obs_dd_total = obs_dd["total"]
        obs_dd_total["num_days"] = obs_dd_total.get("num_days", 0) + 1
        obs_dd_total[
            "fill_ratio"] = obs_dd_total["num_days"] / stats["total_days"]

        obs_dd_type = obs_dd[filetype]
        obs_dd_type["num_days"] = obs_dd_type.get("num_days", 0) + 1
        obs_dd_type[
            "fill_ratio"] = obs_dd_type["num_days"] / stats["total_days"]

    return dict(stats)


if __name__ == "__main__":
    sys.exit(main())
