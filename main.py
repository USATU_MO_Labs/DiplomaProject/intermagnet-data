import sys
import itertools
from pathlib import Path
from src.database import DatabaseService, Grid
from src.filereader import FileReader
from src.interpolation import generate_grid

def _open_db():
    return DatabaseService(Path(__file__).parent / 'database.db')

def main():
    dbs = _open_db()
    file_reader = FileReader()

    root = Path(__file__).parent / '../../ftp.seismo.nrcan.gc.ca'

    for filepath in itertools.chain(root.glob('**/*.min'),
                                    root.glob('**/*.cdf')):
        print(filepath.name)
        processed = file_reader.process_file(filepath)
        dbs.insert_file(processed)

    dbs.close()

    return 0


def generate_grids(grid_step: int):
    dbs = _open_db()

    observatories = {}
    for o in dbs.get_observatories():
        observatories[o.iaga_code] = o

    dates = dbs.get_time_series_dates()

    for date in dates:
        print(date)
        time_series = dbs.get_time_series(date.year, date.month, date.day,
                                          date.hour)

        longitudes = []
        latitudes = []
        xs = []
        ys = []
        zs = []
        fs = []
        for item in time_series:
            observatory = observatories[item.iaga_code.upper()]
            longitudes.append(observatory.longitude)
            latitudes.append(observatory.colatitude)
            xs.append(item.X)
            ys.append(item.Y)
            zs.append(item.Z)
            fs.append(item.F)

        for vals, component in [(xs, "x"), (ys, "y"), (zs, "z"), (fs, "f")]:
            g, ss, = generate_grid(longitudes, latitudes, vals, grid_step)
            grid = Grid(year=date.year,
                        month=date.month,
                        day=date.day,
                        hour=date.hour,
                        component=component,
                        grid=g,
                        grid_var=ss)

            dbs.insert_grid(grid)

    dbs.close()

    return 0


def plot_grid(year, month, day, hour):
    import matplotlib
    import matplotlib.pyplot as plt
    from matplotlib.colors import BoundaryNorm
    from matplotlib.ticker import MaxNLocator
    import numpy as np

    def create_plot(fig, ax, grid, title):
        levels = MaxNLocator(nbins=15).tick_values(grid.min(), grid.max())

        # pick the desired colormap, sensible levels, and define a normalization
        # instance which takes data values and translates those into levels.
        cmap = plt.get_cmap('PiYG')
        norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

        grid_plot = ax.pcolormesh(grid, cmap=cmap, norm=norm)

        fig.colorbar(grid_plot, ax=ax)
        ax.set_title(title)

    dbs = _open_db()

    for component in ["x", "y", "z", "f"]:
        grids = dbs.get_grid(year, month, day, hour, component)

        grid = np.array(grids.grid)
        grid_var = np.array(grids.grid_var)

        fig, (ax0, ax1) = plt.subplots(nrows=2)
        create_plot(fig, ax0, grid, component + ' grid')
        create_plot(fig, ax1, grid_var, component + ' grid variance')

        # adjust spacing between subplots so `ax1` title and `ax0` tick labels
        # don't overlap
        fig.tight_layout()

    plt.show()
    # plt.savefig("{}-{}-{}-{}-{}.png".format(year, month, day, hour, component))


if __name__ == "__main__":
    # sys.exit(main())
    # sys.exit(generate_grids(1))
    plot_grid(2018, 1, 1, 0)
